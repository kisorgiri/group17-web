import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { AuthService } from '../services/auth.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  token: string;
  user;
  submitting: boolean = false;
  constructor(
    public activeRoute: ActivatedRoute,
    public msgService: MsgService,
    public router: Router,
    public authSerivce: AuthService
  ) {
    this.token = this.activeRoute.snapshot.params['token'];
    console.log('token>>>', this.token);
  }

  ngOnInit() {
    this.user = new User({});

  }

  submit() {
    this.submitting = true;

    this.authSerivce.resetPassword(this.token, this.user).subscribe(
      data => {
        this.msgService.showInfo('Password reset successfull please login');
        this.router.navigate(['/auth/login']);
      },
      error => {
        this.msgService.showError(error);
        this.submitting = false;
      }
    )
  }

}
