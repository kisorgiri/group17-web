import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { MsgService } from 'src/app/shared/services/msg.service';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html', // view
    styleUrls: ['login.component.css'] // styles
})
export class LoginComponent implements OnInit {
    // controller
    public submitting: boolean = false;
    user;

    constructor(
        public router: Router,
        public msgService: MsgService,
        public authService: AuthService
    ) {
        this.user = new User({});
        console.log('constructor block')
    }

    ngOnInit() {
        console.log('on init block');
    }

    login() {
        this.submitting = true;
        this.authService.login(this.user)
            .subscribe(
                (data: any) => {
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('user', JSON.stringify(data.user));
                    this.msgService.showSuccess(`Welcome ${data.user.username}`);
                    this.router.navigate(['/user/dashboard']);
                },
                err => {
                    this.msgService.showError(err);
                    this.submitting = false;
                }
            )

    }
    // asume this is service code
    sendLoginData(data: User) {
        // http call
    }
}
