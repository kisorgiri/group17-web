import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msg.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  submitting: boolean = false;
  user;
  constructor(
    public router: Router,
    public authSerivce: AuthService,
    public msgService: MsgService
  ) {
    this.user = new User({});
  }

  ngOnInit() {
  }
  submit() {
    this.submitting = true;
    this.authSerivce.forgotPassword(this.user.email).subscribe(
      data => {
        this.msgService.showInfo('Password reset link sent to email please check your inbox');
        this.router.navigate(['/auth/login']);
      },
      error => {
        this.msgService.showError(error);
        this.submitting = false;
      }
    )
  }

}
