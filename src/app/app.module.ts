import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routing';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { UsersModule } from './users/users.module';
import { ToastrModule } from 'ngx-toastr';
import { ProductsModule } from './products/products.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule, //  browsersModule is used to render our app in browser and only root module will have browsermodule
    AppRoutingModule,
    AuthModule,
    SharedModule,
    UsersModule,
    ProductsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// glossary////
// module
// component
// services
// pipe
// directives

// decorator
// meta data
// selector

//most frequently used term in angular

// root module helps to
//  1st specify root component


// decorator
// decorator is a function that is used to define class  using meta data  
// decorator function comes with @prefix

// meta data
// object supplied to decorator function is meta data
// meta data varries from decorator to decorator
// meta data for NgModule
// declarations // all the pipe,directives and component are added in declaration block
// imports // imports section always holds modules
// modules can be three types
// 1 third party module
// 2 inbuilt moudle
// 3 own module(submodule)
// exports // anything that module share with another module are kept in exports (pipe, component, directives)
// entryComponents //  TODO visit again after learning routing
// providers // services are kept in providers(providers have global scope)
// bootstrap // this property is available root module only and it is used to specify root component
// 

// directive are attribute or element or class which can be used in html file which will change the behaviour of html

// summary
// entire workflow
// main.ts >>> rootModule >>> root Componet >> index html
// change root module 
// change root component

// forms module (imports) //supply garne directive
// template variable (concept)
// inclue bootstrap

// data binding (communication between view and controller)
// form validation
// 


// data binding
// communication between view and controller
// three ways of data binding
// 1 event binding (event_name)="expresion_to_be_called"
// 2 property binding [property_name] ="condition"
// 3 two way data binding syncronization of data between view and controller