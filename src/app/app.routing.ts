import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';


const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'user',
        loadChildren: './users/users.module#UsersModule'
    },
    {
        path: 'product',
        loadChildren: './products/products.module#ProductsModule'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}


// rotuermodule ==> imports ma rakhne forRoot
// config block :Routes
// supply config block in forRoot method
// 
// directives used
// router-outlet ==> placeholder for configured routes
// routerLink ==> similar to href but angular way to navigate
// router insatance and method navigate to navigate from controller
// eg this.router.navigate(['/path'])

// query paramter as part of url
// activateRoute.snapshot.params.my_placeholder_value
// eg if path:'/abc/:hi ==> // activateRoute.snapshot.params.hi

// query paramater as optional value
// this.activatedRoute.queryParams.subscribe((data)=>{
    // console.log('value >>',data)
// })
