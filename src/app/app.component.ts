import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', // separate file 
  styleUrls: ['./app.component.css'] // seprate file
})
export class AppComponent {
  title = 'group17-web';
  user;
  constructor(public router: Router) {
    this.user = JSON.parse(localStorage.getItem('user'));

  }

  isLoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }



}


// meta data of @component decorator
// selector // this will hold a keyword that can be used as html element in any view file
// ususage
// <app-root></app-root>

// component is group of view controller and styles

// templateUrl: or template // this will specify the html file
// styleUrls or styles will specify style for given component
// class specified by component decorator is controller of that component