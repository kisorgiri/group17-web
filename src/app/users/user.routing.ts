import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
const userRotueConfig: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(userRotueConfig)],
    exports: [RouterModule]
})
export class UserRoutingModule {

}